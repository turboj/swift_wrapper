// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "swift_wrapper",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .executable(
            name: "swift_wrapper",
            targets: ["swift_wrapper"]),
        .library(
            name: "cwrapper",
            targets: ["cwrapper"])
    ],
    dependencies: [
        .package(url: "https://turboj@bitbucket.org/turboj/cpplib.git", .branch("master"))
    ],
    targets: [
        .target(name: "cwrapper",
            dependencies: ["cpplib"],
            path: "./Sources/cwrapper"
        ),
        .target(name: "swift_wrapper", 
            dependencies: ["cwrapper"]
        )
    ]
)
